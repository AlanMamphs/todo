from django.urls import path
from .views import *

urlpatterns = [
    path('', ToDoListCreateAPIView.as_view(), name='todos'),
    path('<int:pk>', ToDoRetrieveUpdateDestroyView.as_view(), name='todo')
]
