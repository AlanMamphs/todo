from django.shortcuts import render
from .serializers import *
# Create your views here.
from rest_framework import generics


class ToDoListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = ToDoSerializer
    queryset = TodoList.objects.all()


class ToDoRetrieveUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ToDoSerializer
    queryset = TodoList.objects.all()
