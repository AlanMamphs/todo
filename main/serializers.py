from rest_framework import serializers
from main.models import TodoList


class ToDoSerializer(serializers.ModelSerializer):
    class Meta:
        model = TodoList
        fields = '__all__'
