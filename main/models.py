from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.
from django.urls import reverse
from django.utils import timezone


class User(AbstractUser):
    pass


class TodoList(models.Model):
    class Meta:
        verbose_name_plural = 'todo'
        verbose_name = 'todo'

    title = models.CharField(max_length=250)
    content = models.TextField(blank=True)
    created = models.DateField(auto_now_add=True)
    due_date = models.DateField()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('todo', args=[str(self.id)])
