from rest_framework.test import APITestCase

from .models import TodoList
from .serializers import ToDoSerializer
from main.utils import ListTestMixin, CreateTestMixin, GetInstanceTestMixin, UpdateTestMixin
from django.urls import reverse


class TodoListTestCase(ListTestMixin, APITestCase):
    model = TodoList
    serializer = ToDoSerializer
    url_name = reverse('todos')


class ToDoDetailTestCase(GetInstanceTestMixin, APITestCase):
    model = TodoList
    serializer = ToDoSerializer

    fixtures = ['main/todo.json']

    def setUp(self):
        self.instance = TodoList.objects.get(pk=1)


class ToDoCreateTestCase(CreateTestMixin, APITestCase):
    model = TodoList
    serializer = ToDoSerializer
    url_name = reverse('todos')
    fixtures = ['main/todo.json']

    request_body = {
        "title": "Fitness",
        "content": "Visit fitness club",
        "due_date": "2019-10-02"
    }


class ToDoUpdateTestMixin(UpdateTestMixin, APITestCase):
    model = TodoList
    serializer = ToDoSerializer
    fixtures = ['main/todo.json']

    def setUp(self):
        self.instance = TodoList.objects.get(pk=1)
        self.update_data = {
            "content":"Hey"
        }
